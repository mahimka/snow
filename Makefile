.PHONY: start
start: clean
	npx webpack-dev-server

.PHONY: lint
lint:
	npx eslint --fix .
	npx prettier --write **/*.css

.PHONY: test
test:
	npx jest --passWithNoTests

.PHONY: build
build: clean
	NODE_ENV=production npx webpack
	mkdir build
	cp -r public/ build
	cp src/index.html dist
	cp -r dist build

.PHONY: publish
publish:
	rsync -avz --chmod=ugo=rwX --del build/ designcenter1.rian.off:/www/data/dc/skost/snow-blowers/
	# <remote> заменить на designcenter1.rian.off:/www/data/dc/ПУТЬ/ДО/ПРОЕКТА/ или др.

.PHONY: release
release: build publish

.PHONY: clean
clean:
	rm -rf dist
	rm -rf build
