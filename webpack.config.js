const path = require('path');
const pkg = require('./package');

const IS_DEV = process.env.NODE_ENV !== 'production';

// базовый конфиг
const config = {
  mode: IS_DEV ? 'development' : 'production',
  devtool: IS_DEV ? 'source-map' : false,

  module: {
    rules: [
      // js
      {
        test: /\.js$/i,
        exclude: /node_modules(?!\/ansi-regex)/,
        loader: 'babel-loader',
        options: { cacheDirectory: true },
      },

      // css
      {
        test: /\.css$/i,
        exclude: /(node_modules|(\/widget)|(\/leaflet))/,
        use: [
          {
            loader: 'style-loader',
            options: { sourceMap: IS_DEV },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: IS_DEV,
              modules: true,
              hashPrefix: 'snow-blowers',
              localIdentName: IS_DEV
                ? '[local]--[hash:base64:5]'
                : '[hash:base64]',
            },
          },
          {
            loader: 'postcss-loader',
            options: { sourceMap: IS_DEV },
          },
        ],
      },

      // widget and leaflet css
      {
        test: /((widget\/index)|leaflet)\.css$/i,
        exclude: /node_modules(?!\/leaflet)/,
        use: [
          {
            loader: 'style-loader',
            options: { sourceMap: IS_DEV },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: IS_DEV,
            },
          },
          {
            loader: 'postcss-loader',
            options: { sourceMap: IS_DEV },
          },
        ],
      },

      // остальное
      {
        exclude: /\.(js|css|json)$/i,
        loader: 'file-loader',
        options: { name: '[name].[hash:4].[ext]' },
      },
    ],
  },
};

module.exports = [
  // бандлы
  Object.assign(
    {
      entry: {
        inject: './src/index.js',
        widget: './src/widget/index.js',
        'widget-fixed-height': './src/widget-fixed-height.js',
        'widget-responsive': './src/widget-responsive.js',
      },

      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        publicPath: IS_DEV ? '/dist/' : pkg.publicPath + 'dist/',
      },

      devServer: {
        contentBase: path.join(__dirname, 'public'),
        watchContentBase: true,
      },
    },
    config
  ),
];
