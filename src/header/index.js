import React from 'react';
import Search from './search';
import s from './index.css';

export default class Header extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      search,
      geojson,
      categories,
      selected,
      selectedStreet,
      onSelect,
      onSelectStreet,
      onUnselectStreet,
    } = this.props;
    return (
      <header className={s.header}>
        <p className={s.headerp}>{`Как быстро уберут снег на вашей улице`}</p>
        <Search
          {...search}
          geojson={geojson.filter(f => f.properties.RenamedSt)}
          selectedStreet={selectedStreet}
          onSelectStreet={onSelectStreet}
          onUnselectStreet={onUnselectStreet}
        />
      </header>
    );
  }
}
