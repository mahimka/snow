import React, { Fragment } from 'react';
import $ from 'react-inner-html';
import s from './index.css';
import cx from 'classnames';

export default class Categories extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { categories, selected, onSelect } = this.props;
    const btnCx = i =>
      cx({
        [`${s.btn}`]: true,
        [`${s['btn-selected']}`]: i == selected,
      });
    return (
      <Fragment>

      <p className={s.label} {...$(categories.title)} />
      <section className={s.switcher}>
        
        <ul className={s.list}>
          {categories.list.map((c, i) => (
            <li
              key={i}
              className={btnCx(i)}
              onClick={e => onSelect(e, i)}
              {...$(c.label)}
            />
          ))}
        </ul>
      </section>
      </Fragment>
    );
  }
}
