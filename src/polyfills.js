// нужны по умолчанию
import 'core-js/fn/object/assign';
import 'core-js/fn/promise';
import 'core-js/fn/array/find';
import 'whatwg-fetch';
import 'classlist.js';

const promises = [];

/*
 ** добавляем полифиллы через
 ** динамиеский импорт с необходимыми проверками
 ** (не забываем прописывать webpackChunkName)
 */

//if (!Array.from) {
//  promises.push(
//    import(/* webpackChunkName: "array-from" */ 'core-js/fn/array/from')
//  );
//}

export default Promise.all(promises);
