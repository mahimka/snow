import React from 'react';
import $ from 'react-inner-html';
import s from './index.css';

export default function Legend({ legend, popup, num, amount }) {
  

  let a = ``
  let opa = 1
  if(num != 0){
    a = `Снег уберут за ` + num + ` ` + amount;
    opa = 0.2
  }

  let st = {opacity: opa}


  return (
    <section className={s.legend}>
      <p className={s.title} {...$(legend.title)} />
      <ul className={s.list}>
        {legend.list.map((item, i) => (
          <li key={i} className={s.item} style={{opacity: ( i == (Math.trunc((num-1)/2)) ) ? 1 : opa }}>
            <div className={s.rect} style={{ backgroundColor: item.color }} />
            <p className={s.label} {...$(item.label)} />
          </li>
        ))}
      </ul>

      <div className={s.table}>
        <span className={s.tspan} {...$(a)} />
        <span className={s.tspan} style={{ marginLeft: ((a==``)?`-25px`:`0`)}} {...$(`От 10 см — +1 день на каждые 5 см`)} />
      </div>

      <div className={s.hr} />
      <p className={s.header} {...$(legend.sources.header)} />
      <p className={s.desc} {...$(legend.sources.desc)} />
    </section>
  );
}
