import 'core-js/fn/object/keys';
import './index.css';
import config from '../../package.json';

const url = 'https://ria.ru/20190125/1549130499.html';
const title = encodeURIComponent('Когда уберут снег на вашей улице');
const description = encodeURIComponent(
  'Как быстро убирают снег на московских улицах — в инфографике Ria.ru'
);

const S = {
  vk: `http://vk.com/share.php?url=${url}&title=${title}&description=${description}&image=${
    config.publicPath
  }/assets/share.png`,
  fb: `http://www.facebook.com/sharer/sharer.php?u=${url}`,
  tg: `https://telegram.me/share/url?url=${url}&text=${title}`,
};

Object.keys(S).forEach(key => {
  document
    .querySelector(`[data-share="${key}"]`)
    .addEventListener('click', () => {
      window.open(
        S[key],
        'share',
        'height=450, width=550, toolbar=0, menubar=0, directories=0, scrollbars=0',
        '__target=blank'
      );
    });
});

document.querySelector(`.enter`).addEventListener('click', () => {
  window.open(url, '__target=blank');
});
