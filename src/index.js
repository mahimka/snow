import polyfills from './polyfills';

/*
 ** грузим общие стили до компонентов,
 ** чтобы не было проблем со specificity
 */
import s from './style/reset.css';
import 'leaflet/dist/leaflet.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import pubPath from './utils/pub-path';
import config from '../package.json';

const curScript = (function() {
  const scripts = Array.from(document.getElementsByTagName('script'));
  const injectName = `${config.publicPath}dist/inject.js`;
  const injectScripts = scripts.filter(x => {
    const src = x.getAttribute('src');
    return src && src.includes(injectName);
  });
  const script = injectScripts[injectScripts.length - 1];
  return script == null ? document.currentScript : script;
})();

const lang = curScript.getAttribute('data-lang') || 'ru';

function main() {
  polyfills.then(init);
}

function init() {
  const container = createContainerBefore(curScript);
  fetch(pubPath(`data/${lang}.json`))
    .then(res => res.json())
    .then(createProps)
    .then(props => {
      ReactDOM.render(<App {...props} />, container);
    });
}

function createProps(data) {
  // обрабатываем данные, если нужно
  // и возвращаем пропсы
  data.geojson = data.geojson
    .map(geo => {
      const LatLon = geo.geometry.coordinates.map(i => {
        if (i.length > 1) {
          i = i.map(j => {
            if (j.length) {
              j = j.reverse();
            }
            return j;
          });
        }
        return i;
      });
      geo.geometry.coordinates = LatLon;
      return geo;
    })
    .sort((x, y) => {
      const first = x.properties.RenamedSt
        ? x.properties.RenamedSt.toUpperCase()
        : '';
      const second = y.properties.RenamedSt
        ? y.properties.RenamedSt.toUpperCase()
        : '';
      return first > second ? 1 : first < second ? -1 : 0;
    });
  return data;
}

function createContainerBefore(node) {
  const div = document.createElement('div');
  const preloader = document.createElement('img');
  preloader.setAttribute('src', pubPath('loader.gif'));
  preloader.setAttribute(
    'style',
    'display:block;max-width:100%; height: auto; margin: 70px auto 0px;'
  );
  const p = document.createElement('p');
  p.innerHTML = 'Идет загрузка...';
  p.setAttribute(
    'style',
    'text-align: center; margin-top:1em; font-family:"Noto Sans", "NotoSans", sans-serif;'
  );
  div.appendChild(preloader);
  div.appendChild(p);

  const container = node.parentNode.insertBefore(div, node);

  container.classList.add(s['ig-root']);

  return container;
}

main();
