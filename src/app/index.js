import React from 'react';
import s from './index.css';
import Intro from '../intro';
// import Authors from '../authors';
import Legend from '../legend';
import Header from '../header';
import Footer from '../footer';
import popupFunc from '../popup';
import L from 'leaflet';
import './../style/leaflet.css';

export default class App extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      blur: false,
      selected: 0,
      selectedStreet: -1
    };

    this.onStart = this.onStart.bind(this);
    this.onBlur = this.onBlur.bind(this);

    this.colors = this.props.legend.list;
    this.popupOpen = false;

    this.vw = window.innerWidth;

    this.prevZoom = 9;
    this.onZoomEnd = this.onZoomEnd.bind(this);
    this.onFade = this.onFade.bind(this);
    this.onReset = this.onReset.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onSelectStreet = this.onSelectStreet.bind(this);
    this.onUnselectStreet = this.onUnselectStreet.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onResize = this.onResize.bind(this);
  }

  componentDidMount() {
    this.map = L.map(s.map, {
      maxBounds: L.latLngBounds(
        L.latLng(56.611017, 35.576032),
        L.latLng(54.92675, 39.525617)
      ),
      attributionControl: false,
      renderer: L.canvas({ tolerance: 15 }),
      scrollWheelZoom: true,
    }).setView([55.755814, 37.617635], window.innerHeight < 700 ? 9 : 10);

    this.map.zoomControl.setPosition('topright');
    this.map.on('zoomend', this.onZoomEnd);
    this.prevZoom = 9;

    L.tileLayer('https://dc.ria.ru/dc/igx/tiles/moscow-blue-2021/{z}/{x}/{y}.png', {
      maxZoom: 15,
      minZoom: 9,
    }).addTo(this.map);

    this.createPolylines();

    window.addEventListener('resize', this.onResize);
  }

  createPolylines() {
    const { popup, categories, geojson } = this.props;
    this.polylines = [];
    geojson.forEach(line => {
      const days =
        categories.list[this.state.selected][line.properties.Category];
      if (days !== undefined) {
        const linePol = L.polyline(line.geometry.coordinates, {
          color: this.colors.find(i => days <= i.max).color,
          weight: 1,
          id:line.properties.id,
        }).addTo(this.map);

        linePol.bindPopup(
          () =>
            popupFunc(
              line,
              categories.list,
              popup.label,
              popup.amounts,
              popup.table
            ),
          {
            maxWidth: 270,
            autoPanPaddingTopLeft: L.point(5, this.vw < 701 ? 5 : 70),
            autoPanPaddingBottomRight: L.point(200, 5),
          }
        );

        const lineObj = {
          id: line.properties.id,
          polyline: linePol,
          category: line.properties.Category,
          latLng: line.geometry.coordinates,
        };
        this.polylines.push(lineObj);

        linePol.off('mousedown');
        linePol.off('click');
        linePol.on('click', this.onClick);
        linePol.on('popupclose', () => this.onReset());
        linePol.on('popupopen', () => this.onFade(lineObj));
      }
    });
  }

  onResize() {
    const vw = window.innerWidth;
    if (vw < 701 ? this.vw > 700 : this.vw < 701)
      this.polylines.forEach(marker => {
        const popup = marker.polyline.getPopup();
        popup.options.autoPanPaddingTopLeft = L.point(5, vw < 701 ? 5 : 70);
        popup.update();
      });
    this.vw = vw;
  }

  onFade(marker) {
    this.popupOpen = true;
    this.polylines.forEach(m => {
      if (marker.id !== m.id) {
        m.polyline.setStyle({
          opacity: 0.5,
        });
      } else {
        m.polyline.setStyle({
          opacity: 1,
        });
      }
    });
  }

  onReset() {
    this.repaint(undefined, true);
    setTimeout(() => {
      this.popupOpen = false;
    }, 0);

    this.setState({ selectedStreet: -1})
  }

  onClick(e) {
    this.setState({ selectedStreet: e.target.options.id})
    if (this.popupOpen) {
      return;
    } else {
      e.target.openPopup();
    }
  }

  repaint(weight, opacity) {
    this.polylines.forEach(marker => {
      const days = this.props.categories.list[this.state.selected][
        marker.category
      ];

      let newStyle = {
        color: this.colors.find(i => days <= i.max).color,
      };
      if (weight) newStyle.weight = weight;
      if (opacity) newStyle.opacity = 1;

      marker.polyline.setStyle(newStyle);
    });
  }

  onZoomEnd(e) {
    const lvl = e.target.getZoom();
    const needRepaint =
      (this.prevZoom < 12 && lvl >= 12) ||
      (this.prevZoom >= 12 && lvl < 12) ||
      (this.prevZoom < 15 && lvl >= 15);

    if (needRepaint) {
      this.repaint(lvl < 12 ? 1 : lvl < 15 ? 3 : 4);
    }
    this.prevZoom = lvl;
  }

  onSelectStreet(street) {
    const marker = this.polylines.find(m => m.id == street.properties.id);
    if (marker) {
      this.map.fitBounds(marker.polyline.getBounds());
      marker.polyline.openPopup();
    }
    this.setState({ selectedStreet: street.properties.id})
  }

  onUnselectStreet(){
    this.map.closePopup();
    this.onReset()
  }

  onStart() {
    this.setState({
      blur: false,
    });
  }

  onBlur() {
    this.setState({
      blur: true,
    });
  }

  onSelect(e, idx) {
    if (e) e.stopPropagation();
    if (this.state.selected != idx)
      this.setState(
        {
          selected: idx,
        },
        () => {
          setTimeout(() => this.repaint(), 0);
        }
      );
  }

  render() {
    const { legend, intro, authors, popup, geojson, categories } = this.props;

    let cat,num,amount;
    if(this.state.selectedStreet!=-1){
      cat = geojson.find(m => m.properties.id == this.state.selectedStreet);
      num = categories.list[this.state.selected][cat.properties.Category]
      amount = num > 4 ? popup.amounts[1] : popup.amounts[0];
    }else{
      num = 0;
      amount=""
    }

    return (
      <article id={s.start} className={s.app}>
        <section className={s.wrap}>
          <section
            className={`${s.content} ${this.state.blur ? s.blured : ''}`}
          >
            
            <Header
              {...this.props}
              mapId={s.map}
              selected={this.state.selected}
              selectedStreet={this.state.selectedStreet}
              onSelect={this.onSelect}
              onSelectStreet={this.onSelectStreet}
              onUnselectStreet={this.onUnselectStreet}
            />

            <article className={s.map}>
              <figure id={s.map} className={s.figure} />
              <button className={s.btn} 
                onClick={this.onBlur}
              ></button>
              {this.state.blur && <Intro {...intro} onStart={this.onStart} />}
            </article>

            <Footer
              {...this.props}
              mapId={s.map}
              selected={this.state.selected}
              selectedStreet={this.state.selectedStreet}
              onSelect={this.onSelect}
              onSelectStreet={this.onSelectStreet}
              legend={legend}
              
              num={num}
              amount={amount}

              popup={popup}
            />

          </section>
          
        </section>

      </article>
    );
  }
}
