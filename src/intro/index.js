import React from 'react';
import $ from 'react-inner-html';
import s from './index.css';

export default function Intro({ title, description, btn, onStart }) {
  return (
    <section className={s.intro} onClick={onStart}>
      <article className={s.wrap} onClick={e => e.stopPropagation()}>
        <h1 className={s.title} {...$(title)} />
        <p className={s.description} {...$(description)} />
        <div className={s.button} onClick={onStart} />
      </article>
    </section>
  );
}
