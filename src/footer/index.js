import React from 'react';
import Categories from '../header/categories';
import s from './index.css';

import Legend from '../legend';

export default class Footer extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      search,
      geojson,
      categories,
      selected,
      onSelect,
      onSelectStreet,
    } = this.props;
    return (
      <footer className={s.footer}>
        <Categories
          categories={categories}
          onSelect={onSelect}
          selected={selected}
        />
      <Legend legend={this.props.legend} popup={this.props.popup} num={this.props.num} amount={this.props.amount}/>
      </footer>
    );
  }
}
