const fs = require('fs');
const path = require('path');
const cmd = require('node-cmd');

const filter = /((\s|\()(от|по|между|c|подъезд|дорога)\s.*\s(до|к|и)(\s|\))|(\s|\S)через(\s|\S)|(\s|\S)вдоль(\s|\S)|пересечение|кинотеатр|квартал|дороги\sобщего\sпользования|внутриквартальные\sдороги|Автостоянка|Автобусный\sкруг|ОДХ|ОРП|ТПУ|путепровод|")/i;
const replace = /(Территория\sу\sстанции|Площадь\sу\sстанции|\(|\)|№)/gi;
// const pp = /(Проектируемый\sпроезд|Пр\.\sпр\.)/i;
const dataPath = path.resolve(__dirname, './tables.json');
let dataObjAll = JSON.parse(fs.readFileSync(dataPath, 'utf8'));

const resultPath = path.resolve(__dirname, `./../../public/data/filtered.json`);
const coordsPath = path.resolve(__dirname, `./../../public/data/coords.json`);
const errorsPath = path.resolve(__dirname, `./../../public/data/errors.json`);
const notFoundPath = path.resolve(
  __dirname,
  `./../../public/data/notfound.json`
);

const filtered = dataObjAll
  .filter(i => !filter.test(i['Наименование ОДХ']))
  .map(i => {
    i['Наименование ОДХ'] = `Москва, ${i['Наименование ОДХ'].replace(
      replace,
      ' '
    )}`;
    return i;
  });

let coordsArray = [];
let errors = [];
let notFound = [];
let count = 0;
let n = filtered.length;
let chunksize = 200;
// let error = 0;
// let notfound = 0;
for (let i = 0; i < n / chunksize; i++) {
  setTimeout(() => {
    const start = chunksize * i;
    const size = start + chunksize > n ? n - start : chunksize;
    filtered.slice(start, start + size).forEach(item => makeRequest(item));
  }, 60000 * (i - 1));
}

const makeRequest = item => {
  const street = item['Наименование ОДХ'];
  const category = item['Категория'];

  cmd.get(
    `curl -X GET 'https://geocode-maps.yandex.ru/1.x/?apikey=90732ece-a9bd-40a2-a48f-e14fba442ce2&format=json&geocode=${street.replace(
      /\s+/g,
      '+'
    )}'`,
    (err, data) => {
      if (!err) {
        const dJson = JSON.parse(data).response.GeoObjectCollection;
        if (dJson.metaDataProperty.GeocoderResponseMetaData.found != '0') {
          let result = dJson.featureMember[0].GeoObject;
          let coords = result.Point.pos.split(' ').map(i => parseFloat(i));
          result.Point.lat = coords[0];
          result.Point.lon = coords[1];
          coordsArray.push({
            street: street,
            coord: result,
            category: category,
          });
          // console.log(coordsArray, coords, result)
          count++;
          if (count == n) {
            fs.writeFileSync(coordsPath, JSON.stringify(coordsArray, null, 2));
            fs.writeFileSync(notFoundPath, JSON.stringify(notFound, null, 2));
            fs.writeFileSync(errorsPath, JSON.stringify(errors, null, 2));
            // console.log(count, error, notfound);
          }
        } else {
          count++;
          // notfound++;
          notFound.push(item);
          if (count == n) {
            fs.writeFileSync(coordsPath, JSON.stringify(coordsArray, null, 2));
            fs.writeFileSync(notFoundPath, JSON.stringify(notFound, null, 2));
            fs.writeFileSync(errorsPath, JSON.stringify(errors, null, 2));
            // console.log('not found', count, error, notfound);
          }
        }
      } else {
        count++;
        // error++;
        errors.push(item);
        if (count == n) {
          fs.writeFileSync(coordsPath, JSON.stringify(coordsArray, null, 2));
          fs.writeFileSync(notFoundPath, JSON.stringify(notFound, null, 2));
          fs.writeFileSync(errorsPath, JSON.stringify(errors, null, 2));
          // console.log('error', count, error, notfound);
        }
      }
    }
  );
};
const result = JSON.stringify(filtered, null, 2);
fs.writeFileSync(resultPath, result);
